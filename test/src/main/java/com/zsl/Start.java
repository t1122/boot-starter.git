package com.zsl;

import com.zsl.bean.EnableRegisterServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableRegisterServer
public class Start {
    public static void main(String[] args) {
        SpringApplication.run(Start.class,args);
    }
}
