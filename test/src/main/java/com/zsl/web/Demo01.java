package com.zsl.web;

import com.zsl.bean.SimpleBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Demo01 {
    @Autowired
    SimpleBean simpleBean;
    @RequestMapping("/a")
    @ResponseBody
    public Object a(){
        return simpleBean;
    }
}
