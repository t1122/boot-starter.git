package com.zsl.config;

import com.zsl.bean.My;
import com.zsl.bean.SimpleBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnBean(My.class)
public class MyAutoConfiguration {
    static {
        System.out.println(" MyAutoConfiguration ....");
    }
    @Bean
    public SimpleBean simpleBean(){
        return new SimpleBean();
    }
}
